# CurrencyConverterExercise

This project consists of an incomplete JavaFX application to perform currency conversions. It uses FXML to specify the view and follows the MVC (Model-View-Controller) architecture. 

The goal of this project is to provide an educational tool to teach Graphical User Interfaces in JavaFX. It has all the GUI elements of the application in place, and most of the functionality has been implemented, but some parts are missing, and the controls in the view have to be connected to appropriate behavior in the controller Java class.

## Description of the GUI

The main interface is a GUI consisting of a TabPane with two tabs: one to perform the **Conversion** operations, and a another to view and edit the **Settings**.

The **Conversion** tab contains the following elements:

1. Input *TextField* to enter the amount of currency to convert, and Output *TextField* to visualize the converted value
2. Input/Output *ComboBox* to select the input/output currency to be used in the conversion
3. Precision *Slider* to select the ammount of decimal positions to show the conversion result in the output *TextField*
4. ToggleButton group with two buttons (Auto & Manual) to choose between the manual and the automatic mode.
5. Convert *Button* to perform the conversion manually, and Clear *Button* to clear the input/output fields.

 
![Currency Conversion tab panel](http://i.imgur.com/KJZ2GSP.png)

The **Settings** tab contains:

1. The list of available currencies (*ListView*)
2. A CheckBox to alternate between editable and non-editable mode
3. TextField to see and edit the conversion rate (in U.S.Dollars) of the selected currency
4. Buttons to add and remove currencies to/from the list of available currencies

![Settings tab](http://i.imgur.com/xYjVCT6.jpg)

## Functional Requirements

1. Initial state: When the application is started for the first time, it shows the Conversion tab, the input field is empty, the input currency is set to the first currency available, and the output currency is set to the last currency available.

2. ToggleButtons Auto y Manual operate in a group, only one can be selected at a time, and it decides the operating mode: automatic or manual. 

	2-1. By default, the manual mode is selected

3. When operating in manual mode the "Convert" button is used to execute a conversion of the input currency into an output currency. 
	
	3-1. The input value is entered into the Input *TextField*, and 

	3-2 the result of the conversion is displayed in the Output *TextField*.

4. When operating in automatic mode, the conversion is computed -and the output value is updated- whenever the input value changes, without having to press the Convert button. As a consequence, the Convert button is totally unnecesary and can be disabled.

5. The Clear button empties the input and output text fields. 

6. The Precision slide sets the number of decimal digits used to show the output of the conversion. 
    
	6-1. By default it should be set to 2.

	6-2. The output value should be updated whenever the precision changes

7. When the Settings are checked for the first time, no currency is selected and the conversion rate (USD Exchange) is empty. 
 
	7-2 By default the editable Checkbox is uncheked and the USD Exchange conversion rate is empty

8. When the Editable checkbox is selected the conversion rate can be edited.
	
	8-1.  If  a currency is selected and it is editable, any editing in the text field will modify the conversion rate of the Currency for the rest of the session.

	8-2. Whenever a currency conversion rate is modified, if the input or output currency uses that currency, then the conversion has be repeated and the output value shall be updated.

9. The Add button adds a new currency from a predefined collection of currencies.
	9-1. a currency is added only if there are still some unused currencies from the predefined collection. the button should be disable when this occcurs
	
10. The Remove button removes one currency from the list of available currencies. Two situations and two different behaviors can occur:
	
	10-1. If a currency is selected, then that currency is the one removed  
 
	10-2. If no currency is selected, then the last currencty in the list is removed

 	10-3. A currency can be removed only if there are at least three currencies available. The Remove button should be disable when this happens      
 
## Images showing various application states

Image showing the initial state

![Initial State](http://i.imgur.com/hkkQMhB.jpg)

Image showing the result of the conversion process

![Conversión de divisas](http://i.imgur.com/XFJT2nB.jpg) 

Image showing the use of the Precision Slide:

![Using the Precision Slider](http://i.imgur.com/DOmMfKS.jpg)

Settings with no currency selected

![Settings with no currency selected](http://i.imgur.com/MQH4Rje.jpg)

Settings when a currency is selected, after having added and removed some other currencies

![Settings with a currency selected](http://i.imgur.com/ezFuI6L.jpg)

## TODO Guide & Check List

### Download and installation

This project can be cloned with `git clone https://github.com/magomar/CurrencyConverterFX.git` or `git clone https://bitbucket.org/magomar/currencyconverterfx.git`

Alternatively it can be downloaded as a zip file from [GitHub](https://github.com/magomar/CurrencyConverterFX.git) or [Bitbucket](https://bitbucket.org/magomar/currencyconverterfx/downloads)

Either clone or download the project as a zip and unzip it anywhere.  The project comes ready to be opened with Eclipse, but you can use your favorite IDE to import it. I doesn't use external libraries or frameworks so configuration will be easy.

Once you have imported the project into your IDE, follow the instructions below. Many of the things to do are also annotated with TODO tags inside the code for easy location.


### Link View controls to its behavior in the Controller

The desired behavior of the application is mostly implemented, but it is not connected to the controls in the View. In this exercise, you have to make the connections between the controls in the View, specified in FXML, and the code in the Controller, in Java. It can be accomplished by editing the fxml file, either using a text editor, or using the **Scene Builder** tool. 

The following controls found in the view need to be linked to the corresponding Action Event handlers implemented in the Controller. In FXML this is specified using the `onAction` property. In the SceneBuilder the hanlder can be specified in *Code > OnAction*.

####Controls in the Converter tab

* Button **convertButton**.
* Button **clearButton**
* ToggleButon **autoToggleButton** y **manualToggleButton**

#### Controls in the Settings tab

* CheckBox **isEditableCheckBox**
* TextField **conversionRateField**" 
* Button **addCurrencyButton** 
* Button **removeCurrencyButton** 

### Add missing change listeners to the Controller

In the Controler

* Add listener to the **inputCurrencyComboBox** to automate conversion when a new Currency is selected
* Add listener to the **outputCurrencyComboBox** to automate conversion when a new Currency is selected
* Add listener to detect changes in the **precisionSlider** and adjust the formatting of the output currency (precision = number of decimal digits shown)
* Add listener to **currencylistView** to detect when a new currency is selected, and then do the following
	* Set visibility of dependent controls
    * Set the right information for the selected currency: full name, conversion rate
    * Enable the removeCurrencyButton if a currency is selected, disable it otherwise
* Add listener to **conversionRateField** to detect changes in the conversion rate. Check whether the new rate is a number, and if that is the case do the following:
	* modify the conversion rate of the selected currency 
    * if needed recompute the conversion considering the new conversion rate