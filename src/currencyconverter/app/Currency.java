package currencyconverter.app;

/**
 * Model class, encapsulates knowledge about a particular Currency, like US Dollars, or British pounds.
 */
public class Currency {

    /**
     * Conversion rate, from this Currency into U.S. Dollars
     */
    private double dollarConversionRate;
    /**
     * Full Currency name
     */
    private String fullName;
    /**
     * Three letters Currency code
     */
    private String shortName;

    public Currency(String shortName, String fullName, double dollarConversionRate) {
        this.dollarConversionRate = dollarConversionRate;
        this.fullName = fullName;
        this.shortName = shortName;
    }

    public double getDollarConversionRate() {
        return dollarConversionRate;
    }

    public void setDollarConversionRate(double dollarConversionRate) {
        this.dollarConversionRate = dollarConversionRate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return shortName;
    }
}
