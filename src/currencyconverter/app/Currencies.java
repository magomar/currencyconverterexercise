package currencyconverter.app;

import java.util.LinkedList;
import java.util.List;

/**
 * Utility class used to provide a dynamic collection of currencies. A small collection of currencies from around the
 * World is defined as a source to obtain a sample of currencies, which is achieved by calling method
 * {@link #getSomeCurrencies}. In addition there are two methods to dinamically add and remove elements to the sample
 * ({@link #addCurrency} and {@link #removeCurrency}).
 */
public class Currencies {
    /**
     * A sample of currencies from the world, it can be augmented or reduced by calling {@link #addCurrency()} and
     * {@link #removeCurrency()}/{@link #removeCurrency(Currency)}
     */
    private static List<Currency> CURRENCIES = new LinkedList<>();
    /**
     * A second sample of currencies that can be moved from/to the {@link #CURRENCIES} list
     */
    private static List<Currency> OTHER_CURRENCIES = new LinkedList<>();

    static {
        CURRENCIES.add(new Currency("USD", "U.S Dollar", 1.0));
        CURRENCIES.add(new Currency("GBP", "Pound Sterling", 1.50642));
        CURRENCIES.add(new Currency("EUR", "Euro", 1.12870));
        OTHER_CURRENCIES.add(new Currency("INR", "Indian rupee", 0.0160995));
        OTHER_CURRENCIES.add(new Currency("AUD", "Australian dollar", 0.776900));
        OTHER_CURRENCIES.add(new Currency("CAD", "Canadian dollar", 0.785361));
        OTHER_CURRENCIES.add(new Currency("SGD", "Singapore dollar", 0.738422));
    }

    private Currencies() {
    }

    /**
     * Obtains a sample of currencies from around the World which can be augmented
     * or reduced easily.
     *
     * @return a list of currencies
     */
    public static List<Currency> getSomeCurrencies() {
        return CURRENCIES;
    }

    /**
     * Takes one currency from {@link #OTHER_CURRENCIES} and adds it into the {@link #CURRENCIES} list
     */
    public static boolean addCurrency() {
        if (!OTHER_CURRENCIES.isEmpty())
            return CURRENCIES.add(OTHER_CURRENCIES.remove(0));
        else return false;
    }

    /**
     * Takes one specific Currency given as parameter from {@link #CURRENCIES} and moves it into the
     * {@link #OTHER_CURRENCIES} list
     *
     * @return true if the given currency can be removed
     */
    public static boolean removeCurrency(Currency currency) {
        if (null != currency && CURRENCIES.size() > 2 && CURRENCIES.contains(currency)) {
            CURRENCIES.remove(currency);
            return OTHER_CURRENCIES.add(currency);
        } else return false;
    }

    /**
     * Takes last Currency from {@link #CURRENCIES} and moves it into the {@link #OTHER_CURRENCIES} list
     *
     * @return true if a currency can be remove
     */
    public static boolean removeCurrency() {
        if (CURRENCIES.size() > 2) {
            Currency currency = CURRENCIES.remove(CURRENCIES.size() - 1);
            return OTHER_CURRENCIES.add(currency);
        } else return false;
    }
    
    /**
     * Informs whether more currencies can be added to the list of available currencies
     * from the list of other currencies
     * @return true if OTHER_CURRENCIES is not empty
     */
    public static boolean canAddCurrencies() {
        return !OTHER_CURRENCIES.isEmpty();
    }

    /**
     * Informs whether one or more currencies can be removed from the list of available currencies
     * and put back to the list of other currencies
     * @return
     */
    public static boolean canRemoveCurrencies() {
        return CURRENCIES.size() > 2;
    }
}
