package currencyconverter.app;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.converter.DoubleStringConverter;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;


/**
 * Main controller class holding all the event handlers and listeners of the main view, specified in view.fxml
 */
public class Controller implements Initializable {

    @FXML
    private ComboBox<Currency> outputCurrencyComboBox;

    @FXML
    private Label currencyNameLabel;

    @FXML
    private Label conversionRateLabel;

    @FXML
    private TextField conversionRateField;

    @FXML
    private CheckBox currencyEditableCheckBox;

    @FXML
    private ComboBox<Currency> inputCurrencyComboBox;

    @FXML
    private TextField outputAmount;

    @FXML
    private Slider precisionSlider;

    @FXML
    private TextField inputAmount;

    @FXML
    private ListView<Currency> currencyListView;

    @FXML
    private ToggleButton autoToggleButton;

    @FXML
    private ToggleButton manualToggleButton;

    @FXML
    private ToggleGroup modeToggleGroup;

    @FXML
    private Button convertButton;

    @FXML
    private Button removeCurrencyButton;
    
    @FXML
    private Button addCurrencyButton;

    /**
     * Utility object to convert from String to double.
     */
    private final static DoubleStringConverter DOUBLE_STRING_CONVERTER = new DoubleStringConverter();
    /**
     * Utility object to convert from double to String using a decimal format.
     */
    private final static DecimalFormat CURRENCY_FORMAT = new DecimalFormat("#0.00");
    /**
     * List of currencies available to the conversion tool
     */
    private final ObservableList<Currency> currencies = FXCollections.observableArrayList();
    /**
     * Decides whether the conversion rate of a Currency can be edited or not
     */
    private boolean isEditable = false;
    /**
     * Listens for changes in the input amount and calls method {@link #convertAction(javafx.event.ActionEvent)} in response
     */
    private final ChangeListener<String> inputAmountChangeListener = (observable, oldValue, newValue) -> convertAction(null);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    	// Grab some currencies and make them available into our app 
        currencies.addAll(Currencies.getSomeCurrencies());
        // Set the currencies as the model of various controls needing them (ComboBox, ListView)
        inputCurrencyComboBox.setItems(currencies);
        outputCurrencyComboBox.setItems(currencies);
        currencyListView.setItems(currencies);
        // Make some initial selections concerning the currencies to use in the conversion
        inputCurrencyComboBox.getSelectionModel().selectFirst();
        outputCurrencyComboBox.getSelectionModel().selectLast();
        // Set the editable state of the conversion rate TextField to its initial state (false)
        conversionRateField.setEditable(isEditable);
        // Set the state of the check box to its initial state (un-checked)
        currencyEditableCheckBox.setSelected(isEditable);
        autoToggleButton.setSelected(false);

        clearAction(null);
        
        // TODO listeners to the inputCurrencyComboBox to automate conversion when a new Currency is selected
 
        // TODO listeners to the outputCurrencyComboBox to automate conversion when a new Currency is selected

        // TODO Add listener to detect changes in the precision slider, so as to adjust the decimal digits in the output currency

        /*
        TODO Add listener to detect when a certain currency is selected in the currencylistView, and do the following
        TODO Set visibility of dependent controls
        TODO Set the right information for the selected currency: full name, conversion rate
        TODO enable the removeCurrencyButton if a currency is selected, disable it otherwise
        */
        
        /*
         TODO Add listener to detect changes in the conversion rate. It is a number do the following
         TODO modify the currency by setting the new conversion rate
         TODO if needed, update the value of the conversion considering the new conversion rate 
         */
    }

    /**
     * Event handler implementing the conversion of the amount of money specified in the  {@link #inputAmount}
     * between the input and output currencies ({@link #inputCurrencyComboBox} and {@link #outputCurrencyComboBox}).
     * Any {@link currencyconverter.app.Currency} object specifies a conversion rate between itself and the U.S.Dollar).
     * The conversion is done in two steps: from  input value to dollars, and then into the output of the given output
     * currency.
     *
     * @param actionEvent
     * @see currencyconverter.app.Currency
     */
    @FXML
    private void convertAction(ActionEvent actionEvent) {
        Currency inputCurrency = inputCurrencyComboBox.getValue();
        Currency outputCurrency = outputCurrencyComboBox.getValue();
        double inputValue;
        if (!inputAmount.getText().equals("") && isNumeric(inputAmount.getText())) {
            inputValue = DOUBLE_STRING_CONVERTER.fromString(inputAmount.getText());
            double inputValueInDollars = inputValue * inputCurrency.getDollarConversionRate();
            double outputValue = inputValueInDollars / outputCurrency.getDollarConversionRate();
            outputAmount.setText(CURRENCY_FORMAT.format(outputValue));
        }
    }

    /**
     * Event handler used to clear the conversion interface from previous values
     *
     * @param actionEvent
     */
    @FXML
    private void clearAction(ActionEvent actionEvent) {
        inputAmount.setText("");
        outputAmount.setText("");
    }

    /**
     * Event handler to change the property of being editable for a Currency in the Settings tab
     *
     * @param actionEvent
     */
    @FXML
    private void switchIsEditable(ActionEvent actionEvent) {
        isEditable = !isEditable;
        conversionRateField.setEditable(isEditable);
    }

    /**
     * Event handler to manage the conversion mode, which can alternate between "automatic" and "manual" modes.
     * In the manual mode, the conversion can only be run by clicking the "Convert" button (or pressing the Enter key).
     * In the automatic mode, a listener is added to field {@link #inputAmount} so as to be notified when the amount
     * changes and act accordingly.
     *
     * @param actionEvent
     * @see #inputAmountChangeListener
     */
    @FXML
    private void switchAutomaticConversion(ActionEvent actionEvent) {
        if (modeToggleGroup.getSelectedToggle() == autoToggleButton) {
            convertButton.setDisable(true);
            //  Add ChangeListener to inputAmount to launch convertAction whenever the input value changes
            inputAmount.textProperty().addListener(inputAmountChangeListener);
            convertAction(null);
        } else {
            convertButton.setDisable(false);
            inputAmount.textProperty().removeListener(inputAmountChangeListener);
        }
    }

    /**
     * Event handler that tries to add a new currency to the list of available currencies
     *
     * @param actionEvent
     */
    @FXML
    private void addCurrency(ActionEvent actionEvent) {
        if (Currencies.addCurrency()) {
            updateCurrencies();
            if (!Currencies.canAddCurrencies()) addCurrencyButton.setDisable(true);
            if (removeCurrencyButton.isDisable()) removeCurrencyButton.setDisable(false);
        }
    }

    /**
     * Event handler that tries to remove a currency from the list of available currencies
     * If a currency is selected in the {@link #currencyListView} it attempts to remove that one.
     * Otherwise, it attempts to remove the last one in the list.
     *
     * @param actionEvent
     */
    @FXML
    private void removeCurrency(ActionEvent actionEvent) {
        Currency selectedCurrency = currencyListView.getSelectionModel().getSelectedItem();
        if (Currencies.removeCurrency(selectedCurrency) || Currencies.removeCurrency()) {
            updateCurrencies();
            if (!Currencies.canRemoveCurrencies()) removeCurrencyButton.setDisable(true);
            if (addCurrencyButton.isDisable()) addCurrencyButton.setDisable(false);
        }
    }

    /**
     * Ensures the list of currencies is updated and re-selects the items of the two combo boxes used to set the input
     * and output currencies. This is needed after adding or removing currencies.
     */
    private void updateCurrencies() {
        currencies.clear();
        currencies.addAll(Currencies.getSomeCurrencies());
        inputCurrencyComboBox.getSelectionModel().selectFirst();
        outputCurrencyComboBox.getSelectionModel().selectLast();
    }

    /**
     * Checks whether a given string satisfies the criteria to be converted into a Java number
     *
     * @param str
     * @return
     */
    private static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
